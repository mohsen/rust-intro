/// Generic Data Types

struct Point_i32 {
    x: i32,
    y: i32
}

struct Point_f64 {
    x: f64,
    y: f64
}

let pi32 = Point_i32 { x: 5, y: 10 };
let pf64 = Point_f64 { x: 1.0, y: 4.0 };

// ∀ T, Point {T, T}
struct Point<T> {
    x: T,
    y: T
}

let pi32 = Point { x: 5, y: 10 };
let pf64 = Point { x: 1.0, y: 4.0 };
