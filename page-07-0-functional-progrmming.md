# Troublemakers

- Global State (Variables)
  + Variable name collisions
  + race-condition/dead-lock in concurrency

- Mutable State
  + Code is much harder to reason
  + Code is more fragile since program reasoning is greatly reduced

