# Why Rust?

- Performance
  + blazing fast: no runtime no gc
  + memory efficient
  + run on embedded device
  + easy integrate with c, c++, python, ...

- Reliability
  + rich type system
  + ownership model to memory/thread safety
  + eliminate bugs on compile-time

- Productivity
  + great doc
  + friendly compiler (you can learn from compiler errors)
  + cargo: package manager and build tool
  + editor support: LSP, auto-complete, auto-formatting

