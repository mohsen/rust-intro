/// # New Type

fn fullname(first: &str, mid: &str, last: &str) -> String {
    first.to_owned() + " " + mid + " " + last
}

/// Imagine we call our Function like:
let (smith, jay, john) = ("Smith", "Jay", "John");
fullname(smith, jay, john);

/// We accidentally put the last name first. How did that happen?
struct First(&str);
struct Mid(&str);
struct Last(&str);

fn fullname(first: First, mid: Mid, last: Last) -> String {
    first.0 + " " + &mid.0 + " " + &last.0
}

let (smith, jay, john) = (First("Smith"), Mid("Jay"), Last("John"));
fullname(smith, jay, john);
fullname(jay, smith, john); // compile error
