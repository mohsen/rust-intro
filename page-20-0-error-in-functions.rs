fn f() -> Result<X, E>
{...}

fn g() -> Result<X, E> {
    // do stuff
    let x = f()?;
    // do other stuffs
}

// equal to:
fn g() -> Result<X, E> {
    // do stuff
    let x = match f() {
        Ok(x) => x,
        Err(e) => return e
    }
    // do other stuffs
}
