fn use_add() -> i32 {
    let x = 10;
    let y = 42;
    add(x, y) 
}

/// Now everything works. But this example is too simplistic. 
/// What would happen if useAdd got passed y:

fn use_add(y: Option<i32>) -> i32 {
    let x = 10;
    add(x, y) // COMPILER ERROR!!
}


fn use_add(y_: Option<i32>) -> Option<i32> { // Type Signature return changed Option
    let x = 10;
    match y_ {                  // using match expretion to pattern matching
        Some(y) => add(x, y)
        None => None            // if we dont get y, i.e None, then we return None
    }
}

/// # Summery
///
/// In the Javascript case, our Value became a NaN which is Isomorphic to Nothing. 
/// The difference being that the Rust compiler enforces the use of Option. 
/// And if we use best practices, Maybe will show up in our Type Signatures, 
/// which will inform us and others to the fact that our Function may fail.
