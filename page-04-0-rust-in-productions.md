# Rust in productions

- Firefox         -- software
- Dropbox         -- cloud
- Cloudflare      -- cloud
- Microsoft Azure -- cloud
- 49              -- embedded
- parity          -- blockchain
- etc ...

# Some Rust features

- Functional paradigm
- OOP paradigm
- Memory Safety
  + move
  + ownership, borrowing
  + lifetime

- Async runtime (tokio: developed by Mozilla, Discord, AWS Lambda and ...)
