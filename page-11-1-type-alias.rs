/// If you’ve worked with languages that have Static Types, e.g. Java, you may have gotten used to complex
/// types such as:
private static final Map<Integer, List<String>> map =
    new HashMap<Integer, List<String>>;

/// sth else in Scala:
class NonEmpty[A](elem: A, left: Set[A], right: Set[A]) extends Set[A] {
    // redacted
}

/// So you can understand why programmers are willing 
/// to abandon Static Types for the simplicity, sth like this:
var map = {}    // javascript
dict = {}       // python

/// # high price to pay for Dynamic Types:
///
/// - Type checking in runtime
/// - Tests to catch Type errors must be written and maintained
/// - Refactoring is so risky that it’s often avoided
/// - Nearly zero help from the IDE
///
/// # downsides of Static Types are
///
/// - Limits the programmer’s freedom
/// - Requires explicit Typing
;
/// solution 1
///
/// # Type Inference
///
/// Type Inference: frees the programmer from explicitly defining Types since the Compiler
/// can usually infer the Type based on its usage.
let x: i32 = 12;
let x = 12;         // still static type
let f: f32 = 12.4;
let f = 12.4;

/// solution 2
///
/// type aliase
type PingRespond = Result<Option<SomeType1, SomeType2<i32>>>;
