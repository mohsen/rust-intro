# Paradigms

- Functional paradigm
  + Immutable data, clone
  + User Data Types
    - type alias (not strong)
    - new type (is strong)
    - ADTs (algebra data types)
    - No Null (a billion dollar mistake)
    - generics
  + Type Class <> Interface <> Trait
  + Iterators, fold, filter, map, ...

- OOP paradigm
