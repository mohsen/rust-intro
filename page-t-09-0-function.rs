/// # Pure Functions
///
/// Let’s look at some simple Mathematical functions:
///
/// f(x) = 25x - 17
/// g(y,z) = 42y + z
/// 
/// - Take one or more inputs 
/// - Perform a computation
/// - Return one result
/// 
/// f(0) = -17
/// g(1,2) = 44
///
/// Functions in Math are very rigorous.
/// unlike some functions on impretive languages ...
/// `do_sth(); // doing side effect`

fn add2(x: i32, y: i32) -> i32 {
    // Implicit return (no semicolon)
    x + y
}
