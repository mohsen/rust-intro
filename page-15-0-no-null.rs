/// # No Null
/// how rust handle it?
/// it's done with `option` type
///
/// follow the example:
enum Option<T> {
    Some(T),
    None
}

struct Person {
    name: String,
    birthdate: Date,
    deathdate: Option<Date>
}

Person {
    name: "Joe Mama".into(),
    birthdate: UTC.ymd(2014, 7, 8),
    deathdate: None
};

