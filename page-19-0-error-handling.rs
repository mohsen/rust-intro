/// # Error handling
///
/// Error Types:
///
/// - recoverable
/// - unrecoverable

let file_result = File::open("hello.txt");

let file = match file_result {
    Ok(file) => file,
    Err(error) => match error {
        // recover error
        NotFound => match File::create("hello.txt") {
            Ok(file) => file,
            Err(e) => panic!("Problem creating the file: {:?}", e),
        },
        // unrecoverable error
        OtherError(e) => {
            panic!("Problem opening the file: {:?}", e);
        }
    },
};
