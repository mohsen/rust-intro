/// It's fine but, you still be wondering how this is better than NULL.
/// This can best be answered by looking at an example in javascript:

const x = 10;
const y; // We did not initialize y
const add = (x, y) => x + y; // Here we try to use y in a computation 
                             // and wind up with NaN. 
let z = add(x, y); // --> NaN, Now any computation that uses z will be NaN

// now in rust:
fn add(x: i32, y: i32) -> i32 {
    x + y
}

fn use_add() -> i32 {
    let x = 10;
    let y = None;
    add(x, y) // COMPILER ERROR!! types incompatible
}

/// The compiler save us from using an incompatible Type. So how do we fix this?
/// We could just initialize y to some Value:
