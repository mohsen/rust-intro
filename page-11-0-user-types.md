# User Types

in functional and rust

Functional              -- Rust
---
- Type Alias            -- type
- Data Type             -- ?
- Algebraic Data Types 
  + Product Type        -- struct
  + Sum Type            -- enum
- New Type              -- tuple-struct
