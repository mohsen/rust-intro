/// # Higer-order function

fn append_if_small(s: String, append: String) -> String {
    if s.len() < 10 {
        s + &append
    } else {
        s
    }
} 

fn append_if_odd_lenght(s: String, append: String) -> String {
    if s.len() % 2 != 0 {
        s + &append
    } else {
        s
    }
}

// Hello World
println!("{}", append_if_small("Hello World".into(), "!!!".into()));
// Hello World!!!
println!("{}", append_if_odd_lenght("Hello World".into(), "!!!".into()));


fn append_if(s: String, append: String) -> String {
    if /*??? WHAT DO I PUT HERE ???*/ {
        s + &append
    } else {
        s
    }
}

fn is_small(s: String) -> bool {
    s.len() < 10
}

fn is_odd(s: String) -> bool {
    s.len() % 2 != 0
}

/// function as a parameter:

fn append_if(
    condition: Fn(String) -> bool, 
    s: String, 
    append: String
) -> String {
    if condition(s) {
        s + &append
    } else {
        s
    }
}

append_if(is_small, "Hello Wrold".into(), "!!!".into());

/// A Higher-order Function is a Function that takes a Function as a Parameter, returns a
/// Function or both.
