# Why Rust?

- Performance
  - blazing fast: no runtime no gc
  - memory efficient
  - run on embedded device
  - easy integrate with c, c++, python, ...

- Reliability
  - rich type system
  - ownership model to memory/thread safety
  - eliminate bugs on compile-time

- Productivity
  - great doc
  - friendly compiler (you can learn from compiler errors)
  - cargo: package manager and build tool
  - editor support: LSP, auto-complete, auto-formatting

# Build it in Rust 

- Command Line (tools)
- WebAssembly (web apps)
  - supercharge javascript
  - blockchain
- Networking (working on servers)
  - performance
  - tiny resource foot-print
  - reliability
- Embedded (run on embedded)
  - low-level control without giving up high-level conveniences
  - portability (no make file)
    - static linking packages
  - unit-test / integrate-test
  - package manager

# Code Example

```rust
fn main() { // entry point
    println!("Hello, world!"); // macro not function
}
```

# Rust in productions

- Firefox -- software
- Dropbox -- cloud
- Cloudflare -- cloud
- Microsoft Azure -- cloud
- 49 -- embedded
- parity -- blockchain
- etc ...

# Contents

- Memory Safty
  - immutable data
  - move
  - ownership, borrowing
  - lifetime
  - clone
- Generics
- Data Types
  - type alias (not strong)
  - new type (is strong)
  - ADT (algebra data type) -- sum type, product type
- No Null (a bilion duller mistake)
- Type Class >> Interface >> Trait
- variable shadowing
- fold, filter, map, ...
- tokio (fast network applications) (async runtime) (developed by Mozilla, Discord, AWS Lambda and ...)

# Lambda Functions

```purescript
\x -> x + 1
```

```purescript
f :: Int -> Int
f = \x -> x + 1
```

Since we’ve named this `f`, there’s no reason to use the Lambda. We could just as easily write f with the
Parameter on the other side of the equal sign:

```purescript
f :: Int -> Int
f x = x + 1
```

So why is this syntax supported. What’s the real benefit of a Lambda?
This syntax is helpful when you need to pass a Function to another Function, but it’s not worthy of a proper
name since it’ll only be used once:

```purescript
filter (\x -> x < 10) [1, 2, 3, 10, 20, 30] -- [1, 2, 3]
```

Here the Lambda Function is a Predicate that checks to see if the Value is less than 10.

# Cargo.toml example
