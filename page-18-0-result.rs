/// # Result
///
/// another failure type
enum Result<T, E> {
    Ok(T),
    Err(E)
}

enum QueryError {
    DatabaseConnectionError(String),
    InvalidQuery,
    NotFound
} 

fn query(q: Query) -> Result<i32, QueryError> 
{ ... }

/// So how would we create the different errors:
Err(DatabaseConnectionError("The connection was refused by the server"))
Err(InvalidQuery)
Err(NotFound)
/// successful result for query:
Ok(42)
