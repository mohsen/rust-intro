/// # Functional view
///
/// No Variables
/// no `x = x + 1` instead use:
let s = 20;           // number of students 
let b = 12;           // students with brown hair
let r = 3;            // students with red hair
let y = s - b - r;    // students with blonde hair
