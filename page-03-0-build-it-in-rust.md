# Build it in Rust 

- Command Line (tools)

- WebAssembly (web apps)
  + supercharge javascript
  + blockchain

- Networking (working on servers)
  + performance
  + tiny resource foot-print
  + reliability

- Embedded (run on embedded)
  + low-level control without giving up high-level conveniences
  + portability (no make file)
    - static linking packages
  + unit-test / integrate-test
  + package manager

