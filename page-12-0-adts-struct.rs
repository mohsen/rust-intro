/// # Product Type
struct Triplet {
    a: String,
    b: i32,
    c: bool
}
/// # Sum type
///
/// in huskell:
data Response = Int | String
/// in C:
union Response {
   int i;
   char* s;
}
/// in Rust:
enum Response {
    Value(i32)
    Error(String),
}
/// in huskell: 
data Response = Value Int | Error String
